<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PlacetoPay - Tienda</title>
    <?php
    echo view('includes/head');
    ?>
</head>

<body>

<?php
echo view('includes/nav');
?>

<!-- Page Content -->
<div class="container margin-top-30">

    <div class="row">

        <div class="col-lg-3">

            <h1 class="my-4">Tienda Online</h1>
            <div class="list-group">
                <a href="<?php echo url('tienda'); ?>" class="list-group-item">Caterogía 1</a>
            </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo url('producto/25.000'); ?>"><img class="card-img-top" src="<?php echo asset('img/productos/producto1.jpg'); ?>" alt=""></a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="<?php echo url('producto/25.000'); ?>">Producto 1</a>
                            </h4>
                            <h5>$25.000</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo url('producto/30.000'); ?>"><img class="card-img-top" src="<?php echo asset('img/productos/producto2.jpg'); ?>" alt=""></a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="<?php echo url('producto/30.000'); ?>">Producto 2</a>
                            </h4>
                            <h5>$30.000</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo url('producto/50.000'); ?>"><img class="card-img-top" src="<?php echo asset('img/productos/producto3.jpg'); ?>" alt=""></a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="<?php echo url('producto/50.000'); ?>">Producto 3</a>
                            </h4>
                            <h5>$50.000</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo url('producto/30.000'); ?>"><img class="card-img-top" src="<?php echo asset('img/productos/producto4.jpg'); ?>" alt=""></a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="<?php echo url('producto/30.000'); ?>">Producto 4</a>
                            </h4>
                            <h5>$30.000</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100">
                        <a href="<?php echo url('producto/24.000'); ?>"><img class="card-img-top" src="<?php echo asset('img/productos/producto5.jpg'); ?>" alt=""></a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="<?php echo url('producto/24.000'); ?>">Producto 5</a>
                            </h4>
                            <h5>$24.000</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur! Lorem ipsum dolor sit amet.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<?php
echo view('includes/footer');
?>
