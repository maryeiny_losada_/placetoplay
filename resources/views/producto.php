<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PlacetoPay - Producto</title>
    <?php
    echo view('includes/head');
    ?>
</head>

<body>

<?php
echo view('includes/nav');
?>

<!-- Page Content -->
<div class="container">

    <div class="row">
        <?php
        if(empty(session('errors')) == FALSE): ?>
            <div class="alert alert-danger">
                <?php
                foreach (session('errors') as $error):
                    foreach ($error as $menssage):
                        echo $menssage.' <br>';
                    endforeach;
                endforeach;
                ?>
            </div>
            <?php
        endif;
        ?>

        <div class="card mt-4">
            <img class="card-img-top img-fluid" src="<?php echo asset('img/productos/productos.jpg'); ?>" alt="">
            <div class="card-body">
                <h3 class="nombre-producto card-title">Producto Test</h3>
                <h4 class="precio-producto">$<?php echo $precio; ?></h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dicta fugit fugiat hic aliquam itaque facere, soluta. Totam id dolores, sint aperiam sequi pariatur praesentium animi perspiciatis molestias iure, ducimus!</p>
                <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
                4.0 stars
            </div>
        </div>
        <!-- /.card -->

        <div class="card card-outline-secondary my-4">
            <div class="card-header">
                Reseñas de productos
            </div>

            <div class="card-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
                <small class="text-muted">Escrito por Anónimo el 3/1/17</small>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
                <small class="text-muted">Escrito por Anónimo el 3/1/17</small>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
                <small class="text-muted">Escrito por Anónimo el 3/1/17</small>
                <hr>
            </div>
        </div>

        <form id="comprar-producto" action="<?php echo url('/comprar'); ?>" method="get" data-toggle="validator">
            <div class="row">
                <input name="precio" class="input-precio" type="hidden" value="<?php echo $precio; ?>" />
                <div class="col-md-12 text-center margin-bottom-15">
                    <button class="btn btn-success" type="submit">
                        Comprar
                    </button>
                </div>
            </div>
            <?php echo csrf_field(); ?>
        </form>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<?php
echo view('includes/footer');
?>
