<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PlacetoPay - Pagar</title>
    <?php
    echo view('includes/head');
    ?>
</head>

<body>

<?php
echo view('includes/nav');
?>

<!-- Page Content -->
<div class="container margin-top-30">
    <div>
        <form id="pago" class="row">
            <div class="form-group col-md-6">
                <label for="banca" class="control-label">Indique el tipo de cuenta</label>
                <select class="form-control" name="banca">
                    <option value="">Persona</option>
                    <option value="">Empresa</option>
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="bancos" class="control-label">Seleccione la entiendad bancaria</label>
                <select class="form-control" name="bancos">
                    <?php
                    foreach ($bancos->getBankListResult->item as $banco): ?>
                        <option value="<?php echo $banco->bankCode; ?>">
                            <?php
                            echo $banco->bankName;
                            ?>
                        </option>
                        <?php
                    endforeach;
                    ?>
                </select>
            </div>
        </form>




        <div class="col-md-12 text-right margin-bottom-30 margin-top-30">
            <a id="comprar-producto" class="btn btn-success">Comprar</a>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<?php
echo view('includes/footer');
?>
