<?php
    $request = null;

    if(empty(session('request')) == FALSE):
        $request = session('request');
    endif;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PlacetoPay - Fin</title>
    <?php
    echo view('includes/head');
    ?>
</head>

<body>

<?php
echo view('includes/nav');
?>

<!-- Page Content -->
<div class="container margin-top-30">
    <div>
        <div class="col-md-12 margin-bottom-30">
            <h3 class="text-center color-ppal">
                Su compra ha finalizado
            </h3>
        </div>

        <?php
            if(empty($error) == FALSE):
        ?>
            <div class="alert alert-danger">
                <?php
                    echo $error.' <br>';
                ?>
            </div>
        <?php
            endif;
        ?>

        <?php
        if(empty($success) == FALSE):
            ?>
            <div class="alert alert-succes">
                <?php
                    echo $success.' <br>';
                ?>
            </div>

        <?php
            endif;
        ?>


        <div class="col-md-12 margin-bottom-30 margin-top-30 ">
            <p class="text-center">
               Gracias por confiar en nosotros.
            </p>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<?php
echo view('includes/footer');
?>
