<?php
    $request = null;

    if(empty(session('request')) == FALSE):
        $request = session('request');
    endif;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PlacetoPay - Lista de Transacciones</title>
    <?php
    echo view('includes/head');
    ?>
</head>

<body>

<?php
echo view('includes/nav');
?>

<!-- Page Content -->
<div class="container margin-top-30">
    <div class="row">
        <div class="col-md-12 margin-bottom-30">
            <h3 class="text-center color-ppal">
                Lista de transacciones
            </h3>
        </div>

        <div class="col-md-12 margin-bottom-30">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">ID transacción</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Fecha de compra</th>
                    <th scope="col">Valor Total</th>
                    <th scope="col">Pagador</th>
                    <th scope="col">Comprador</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if(empty($compras) == FALSE):
                ?>
                    <?php
                    foreach ($compras as $compra): ?>
                        <tr>
                            <th><?php  echo $compra->transactionID; ?></th>
                            <td><?php  echo $compra->responseReasonText; ?></td>
                            <td><?php  echo $compra->date; ?></td>
                            <td>$<?php  echo $compra->totalAmount; ?></td>
                            <td><?php  echo $compra->payer_name; ?></td>
                            <td><?php  echo $compra->buyer_name; ?></td>
                        </tr>
                    <?php
                        endforeach;
                    ?>
                <?php
                    else:
                ?>
                <tr>
                    <th colspan="6">No existen compras disponibles</th>
                </tr>
                <?php
                    endif;
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<?php
echo view('includes/footer');
?>
