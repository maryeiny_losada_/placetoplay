<?php
    $request = null;

    if(empty(session('request')) == FALSE):
        $request = session('request');
    endif;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PlacetoPay - Comprar</title>
    <?php
    echo view('includes/head');
    ?>
</head>

<body>

<?php
echo view('includes/nav');
?>

<!-- Page Content -->
<div class="container margin-top-30">
    <div>
        <?php
            if(empty(session('errors')) == FALSE): ?>
            <div class="alert alert-danger">
                <?php
                foreach (session('errors') as $error):
                    foreach ($error as $menssage):
                        echo $menssage.' <br>';
                    endforeach;
                endforeach;
                ?>
            </div>
        <?php
            endif;
        ?>

        <form id="pago" action="<?php echo url('/confirmar-compra'); ?>" class="row" method="get" data-toggle="validator" role="form">
            <div class="col-md-12 margin-bottom-30">
                <h3 class="text-center color-ppal">
                    Carrito de Compra
                </h3>
            </div>

            <div class="col-md-12 margin-bottom-30">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Producto</th>
                        <th scope="col">Precio</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Producto Test</th>
                        <td><?php echo $datos['precio']; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">TOTAL</th>
                        <td><?php echo $datos['precio']; ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 margin-bottom-30">
                <h5 class="text-center color-ppal">Complete la siguiente información para realizar la compra</h5>
            </div>

            <div id="accordion" class="col-md-12">
                <h3>Pagador</h3>
                <div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="documentType" class="control-label">Tipo de documento de identificación</label>
                            <select class="form-control payer" data-input="documentType" id="payer-documentType" name="payer[documentType]">
                                <option <?php echo $request['payer']['documentType'] == 'CC' ? 'selected' : ''; ?> value="CC">
                                    Cédula de ciudanía colombiana
                                </option>
                                <option <?php echo $request['payer']['documentType'] == 'CE' ? 'selected' : ''; ?> value="CE">
                                    Cédula de extranjería
                                </option>
                                <option <?php echo $request['payer']['documentType'] == 'TI' ? 'selected' : ''; ?> value="TI">
                                    Tarjeta de identidad
                                </option>
                                <option <?php echo $request['payer']['documentType'] == 'PPN' ? 'selected' : ''; ?> value="PPN">
                                    Pasaporte
                                </option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="document" class="control-label">Número de identicación</label>
                            <input type="text" class="form-control payer" maxlength="12" value="<?php echo empty($request['payer']['document']) == FALSE ? $request['payer']['document'] : ''; ?>" data-input="document" id="payer-document" name="payer[document]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="firstName" class="control-label">Nombres</label>
                            <input type="text" class="form-control payer" maxlength="60" value="<?php echo empty($request['payer']['firstName']) == FALSE ? $request['payer']['firstName'] : ''; ?>" data-input="firstName" id="payer-firstName" name="payer[firstName]"
                                   required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="lastName" class="control-label">Apellidos</label>
                            <input type="text" class="form-control payer" maxlength="60" value="<?php echo empty($request['payer']['lastName']) == FALSE ? $request['payer']['lastName'] : ''; ?>" data-input="lastName" id="payer-lastName" name="payer[lastName]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="company" class="control-label">Empresa donde labora</label>
                            <input type="text" class="form-control payer" maxlength="60" value="<?php echo empty($request['payer']['company']) == FALSE ? $request['payer']['company'] : ''; ?>" data-input="company" id="payer-company" name="payer[company]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="emailAddress" class="control-label">Correo electrónico</label>
                            <input type="email" class="form-control payer" maxlength="80" value="<?php echo empty($request['payer']['emailAddress']) == FALSE ? $request['payer']['emailAddress'] : ''; ?>" data-input="emailAddress" id="payer-emailAddress" name="payer[emailAddress]"
                                   required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="address" class="control-label">Dirección</label>
                            <input type="text" class="form-control payer" maxlength="100" value="<?php echo empty($request['payer']['address']) == FALSE ? $request['payer']['address'] : ''; ?>" data-input="address" id="payer-address" name="payer[address]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="city" class="control-label">Ciudad</label>
                            <input type="text" class="form-control payer" maxlength="50" value="<?php echo empty($request['payer']['city']) == FALSE ? $request['payer']['city'] : ''; ?>" data-input="city" id="payer-city" name="payer[city]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="province" class="control-label">Departamento</label>
                            <input type="text" class="form-control payer" maxlength="50" value="<?php echo empty($request['payer']['province']) == FALSE ? $request['payer']['province'] : ''; ?>" data-input="province" id="payer-province" name="payer[province]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="country" class="control-label">País</label>
                            <select class="form-control payer" data-input="country" id="payer-country" name="payer[country]">
                                <?php
                                foreach ($paises as $pais): ?>
                                    <option <?php echo $request['payer']['country'] == $pais->iso2 ? 'selected' : ''; ?> value="<?php echo $pais->iso2; ?>">
                                        <?php
                                        echo $pais->nombre;
                                        ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="phone" class="control-label">Teléfono fijo</label>
                            <input type="text" class="form-control payer" maxlength="30" value="<?php echo empty($request['payer']['phone']) == FALSE ? $request['payer']['phone'] : ''; ?>" data-input="phone" id="payer-phone" name="payer[phone]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="mobile" class="control-label">Teléfono móvil</label>
                            <input type="text" class="form-control payer" maxlength="30" value="<?php echo empty($request['payer']['mobile']) == FALSE ? $request['payer']['mobile'] : ''; ?>" data-input="mobile" id="payer-mobile" name="payer[mobile]" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <h3>Comprador</h3>
                <div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="clonar-info">
                                Usar la misma información del Pagador
                            </label>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="row div-comprador">
                        <div class="form-group col-md-6">
                            <label for="documentType" class="control-label">Tipo de documento de identificación</label>
                            <select class="form-control buyer" id="buyer-documentType" name="buyer[documentType]">
                                <option <?php echo $request['buyer']['documentType'] == 'CC' ? 'selected' : ''; ?> value="CC">
                                    Cédula de ciudanía colombiana
                                </option>
                                <option <?php echo $request['buyer']['documentType'] == 'CE' ? 'selected' : ''; ?> value="CE">
                                    Cédula de extranjería
                                </option>
                                <option <?php echo $request['buyer']['documentType'] == 'TI' ? 'selected' : ''; ?> value="TI">
                                    Tarjeta de identidad
                                </option>
                                <option <?php echo $request['buyer']['documentType'] == 'PPN' ? 'selected' : ''; ?> value="PPN">
                                    Pasaporte
                                </option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="document" class="control-label">Número de identicación</label>
                            <input type="text" class="form-control buyer" maxlength="12" value="<?php echo empty($request['buyer']['document']) == FALSE ? $request['buyer']['document'] : ''; ?>" id="buyer-document" name="buyer[document]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="firstName" class="control-label">Nombres</label>
                            <input type="text" class="form-control buyer" maxlength="60" value="<?php echo empty($request['buyer']['firstName']) == FALSE ? $request['buyer']['firstName'] : ''; ?>" id="buyer-firstName" name="buyer[firstName]"
                                   required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="lastName" class="control-label">Apellidos</label>
                            <input type="text" class="form-control buyer" maxlength="60" value="<?php echo empty($request['buyer']['lastName']) == FALSE ? $request['buyer']['lastName'] : ''; ?>" id="buyer-lastName" name="buyer[lastName]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="company" class="control-label">Empresa donde labora</label>
                            <input type="text" class="form-control buyer" maxlength="60" value="<?php echo empty($request['buyer']['company']) == FALSE ? $request['buyer']['company'] : ''; ?>" id="buyer-company" name="buyer[company]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="emailAddress" class="control-label">Correo electrónico</label>
                            <input type="email" class="form-control buyer" maxlength="80" value="<?php echo empty($request['buyer']['emailAddress']) == FALSE ? $request['buyer']['emailAddress'] : ''; ?>" id="buyer-emailAddress" name="buyer[emailAddress]"
                                   required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="address" class="control-label">Dirección</label>
                            <input type="text" class="form-control buyer" maxlength="100" value="<?php echo empty($request['buyer']['address']) == FALSE ? $request['buyer']['address'] : ''; ?>" id="buyer-address" name="buyer[address]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="city" class="control-label">Ciudad</label>
                            <input type="text" class="form-control buyer" maxlength="50" value="<?php echo empty($request['buyer']['city']) == FALSE ? $request['buyer']['city'] : ''; ?>" id="buyer-city" name="buyer[city]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="province" class="control-label">Departamento</label>
                            <input type="text" class="form-control buyer" maxlength="50" value="<?php echo empty($request['buyer']['province']) == FALSE ? $request['buyer']['province'] : ''; ?>" id="buyer-province" name="buyer[province]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="country" class="control-label">País</label>
                            <select class="form-control buyer" id="buyer-country" name="buyer[country]">
                                <?php
                                foreach ($paises as $pais): ?>
                                    <option <?php echo $request['buyer']['country'] == $pais->iso2 ? 'selected' : ''; ?> value="<?php echo $pais->iso2; ?>">
                                        <?php
                                        echo $pais->nombre;
                                        ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="phone" class="control-label">Teléfono fijo</label>
                            <input type="text" class="form-control buyer" maxlength="30" value="<?php echo empty($request['buyer']['phone']) == FALSE ? $request['buyer']['phone'] : ''; ?>" id="buyer-phone" name="buyer[phone]" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="mobile" class="control-label">Teléfono móvil</label>
                            <input type="text" class="form-control buyer" maxlength="30" value="<?php echo empty($request['buyer']['mobile']) == FALSE ? $request['buyer']['mobile'] : ''; ?>" id="buyer-mobile" name="buyer[mobile]" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6 margin-top-30">
                <label for="bankInterface" class="control-label">Indique el tipo de cuenta</label>
                <select class="form-control" name="bankInterface">
                    <option value="0" <?php echo $request['bankInterface'] == 0 ? 'selected' : ''; ?>>Personas</option>
                    <option value="1" <?php echo $request['bankInterface'] == 1 ? 'selected' : ''; ?>>Empresas</option>
                </select>
            </div>

            <div class="form-group col-md-6 margin-top-30">
                <label for="bankCode" class="control-label">Seleccione la entiendad bancaria</label>
                <select class="form-control" name="bankCode" required>
                    <?php
                    foreach ($bancos->getBankListResult->item as $banco): ?>
                        <option <?php echo $request['bankCode'] == $banco->bankCode ? 'selected' : ''; ?> value="<?php echo $banco->bankCode; ?>">
                            <?php
                            echo $banco->bankName;
                            ?>
                        </option>
                        <?php
                    endforeach;
                    ?>
                </select>
                <div class="help-block with-errors"></div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="medio_pago" class="control-label">Seleccione el método de pago</label>
                    <div class="form-check">
                        <input value="pse" type="checkbox" required class="form-check-input" data-error="Campo obligatorio">
                        <img class="ico-pse" src="<?php echo asset('/img/pse.png'); ?>" />
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
            </div>

            <div>
                <?php
                    $precio = str_replace(".", "", $datos['precio']);
                    $precio = (float)$precio;
                    $taxAmount = $precio*0.19;
                    $devolutionBase = $precio-$taxAmount;
                ?>
                <input type="hidden" name="description" required value="Pago del producto Test por medio de PlacetoPay.">
                <input type="hidden" name="reference" required value="<?php echo 'PLACETOPAY_'.time(); ?>">
                <input type="hidden" name="returnURL" required value="http://localhost/pruebas/placetoplay/public/respuesta-compra">
                <input type="hidden" name="language" required value="ES">
                <input type="hidden" name="currency" value="COP">
                <input type="hidden" name="totalAmount" required value="<?php echo $precio;?>">
                <input type="hidden" name="taxAmount" required value="<?php echo $taxAmount;?>">
                <input type="hidden" name="devolutionBase" required value="<?php echo $devolutionBase;?>">
                <input type="hidden" name="tipAmount" required value="0.0">
                <input type="hidden" name="ipAddress" required value="<?php echo EnvatoFunctions::getIp();?>">
                <input type="hidden" name="userAgent" required value="<?php echo $_SERVER['HTTP_USER_AGENT']; ?>">
            </div>

            <?php echo csrf_field(); ?>

            <div class="col-md-12 text-right margin-bottom-30 margin-top-30">
                <button type="submit" class="btn btn-success">Pagar</button>
            </div>
        </form>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<?php
echo view('includes/footer');
?>
