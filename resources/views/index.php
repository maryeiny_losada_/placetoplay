<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PlacetoPay</title>
    <?php
    echo view('includes/head');
    ?>
</head>

<body>

<?php
    echo view('includes/nav');
?>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div id="carouselExampleIndicators" class="carousel slide col-lg-12" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img class="d-block img-fluid" src="<?php echo asset('img/slider/slider1.jpg'); ?>" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?php echo asset('img/slider/slider2.jpg'); ?>" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?php echo asset('img/slider/slider3.jpg'); ?>" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="col-lg-12 text-center margin-top-30">
            <h1>
                <span><b>Así</b></span> </br>
                te ayuda PlacetoPay
            </h1>

            <p>
                Procesaremos tus transacciones de forma rápida y segura para ti y tus clientes, siendo tus aliados, ofreciéndote soluciones de fácil integración para que vendas tus productos, tus servicios y recaudes tu dinero.
            </p>

            <p>
                Somos más que un botón de pagos, por eso te acompañamos en todo el proceso de la transacción:
            </p>
        </div>

        <div class="col-lg-4 text-center margin-top-30">
            <img src="<?php echo asset('img/iconos/antes.jpg'); ?>"/>

            <p>
                Autenticando, validando y brindandote soluciones técnicas
            </p>
        </div>

        <div class="col-lg-4 text-center margin-top-30">
            <img src="<?php echo asset('img/iconos/pago.jpg'); ?>"/>

            <p>
                Verificando, calificando y filtrando los pagos
            </p>
        </div>

        <div class="col-lg-4 text-center margin-top-30">
            <img src="<?php echo asset('img/iconos/despues.jpg'); ?>"/>

            <p>
                Procesando, conciliando y almacenando tus transacciones
            </p>
        </div>

        <div class="col-lg-12 text-center margin-top-30">
            <h4>
                Vendes más con nosotros
            </h4>
            <h5>
                porque tienes múltiples canales para recibir tus pagos
            </h5>
        </div>

        <div class="col-lg-3 text-center margin-top-30">
            <img src="<?php echo asset('img/iconos/sitio-web.jpg'); ?>"/>
        </div>

        <div class="col-lg-3 text-center margin-top-30">
            <img src="<?php echo asset('img/iconos/app.jpg'); ?>"/>
        </div>

        <div class="col-lg-3 text-center margin-top-30">
            <img src="<?php echo asset('img/iconos/redes-sociales.jpg'); ?>"/>
        </div>

        <div class="col-lg-3 text-center margin-top-30">
            <img src="<?php echo asset('img/iconos/telefono.jpg'); ?>"/>
        </div>

        <div class="col-lg-12 text-center margin-top-30 margin-bottom-30">
            <a href="<?php echo url('tienda'); ?>" class="btn color-gray">
                Ir a la Tienda
            </a>
        </div>
    </div>
</div>

<?php
    echo view('includes/footer');
?>
