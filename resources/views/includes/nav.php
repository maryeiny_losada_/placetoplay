<!-- Navigation -->
<nav id="nav-ppal" class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="<?php echo url('/'); ?>">
            <img class="logo-nav" src="<?php echo asset('img/logo.png'); ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?php echo Request::is('/') ? 'active' : ''; ?>">
                    <a class="nav-link" href="<?php echo url('/'); ?>">Inicio</a>
                </li>
                <li class="nav-item <?php echo Request::is('tienda') ? 'active' : ''; ?>">
                    <a class="nav-link" href="<?php echo url('tienda'); ?>">Tienda</a>
                </li>
                <li class="nav-item <?php echo Request::is('contactenos') ? 'active' : ''; ?>">
                    <a class="nav-link" href="<?php echo url('contactenos'); ?>">Contáctanos</a>
                </li>
            </ul>
            <a href="<?php echo url('administrador'); ?>">
                <button class="btn btn-nav navbar-btn">
                    Administrador
                </button>
            </a>
        </div>
    </div>
</nav>