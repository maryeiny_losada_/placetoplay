<link rel="shortcut icon" href="<?php echo asset('/img/favicon.ico'); ?>" type="image/vnd.microsoft.icon" />
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="<?php echo asset('vendor/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
<link href="<?php echo asset('css/shop-homepage.css'); ?>" rel="stylesheet">
<link href="<?php echo asset('css/style.css'); ?>" rel="stylesheet">