<div id="loader" class="display-none">
    <div class="loader"></div>
    <div class="load-text margin-top-25 text-center">
        <h5 class="color-red">Procesando compra...</h5>
    </div>
</div>


<!-- Footer -->
<footer class="py-5 bg-dark color-white">
    <div class="border-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center">
                    <p>
                        <b>
                            PlacetoPay transacciones integrales en territorio seguro
                        </b>
                    </p>
                    <ul class="list-unstyled">
                        <li>
                            Teléfono: +57 (4) 444 23 10
                        </li>
                        <li>
                            comercial@placetopay.com
                        </li>
                        <li>
                            Carrera 65 #45 - 20 oficina 430
                        </li>
                        <li>
                            Medellín - Colombia
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 no-padding text-center">
                            <p>
                                <b>
                                    PlacetoPay
                                </b>
                            </p>
                            <ul class="list-unstyled">
                                <li>
                                    <a class="color-white" href="https://www.placetopay.com/web/soporte">
                                        Te ayudamos
                                    </a>
                                </li>
                                <li>
                                    <a class="color-white" href="https://www.placetopay.com/web/soporte">
                                        Conócenos
                                    </a>
                                </li>
                                <li>
                                    <a class="color-white" href="https://www.placetopay.com/web/actualidad">
                                        Blog
                                    </a>
                                </li>
                                <li>
                                    <a class="color-white" href="https://www.placetopay.com/web/trabaja_con_nosotros">
                                        Trabaja con nosotros
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 no-padding text-center">
                            <p>
                                <b>
                                    Legal
                                </b>
                            </p>
                            <ul class="list-unstyled">
                                <li>
                                    <a class="color-white" href="https://www.placetopay.com/web/sites/default/files/2017-11/politicas.pdf">Políticas de protección</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="col-md-12">
            <p class="m-0 text-center text-white">Copyright &copy; PlacetoPay 2018</p>
        </div>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo asset('vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?php echo asset('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo asset('js/validator.js'); ?>"></script>
<script src="<?php echo asset('js/main.js'); ?>"></script>
</body>

</html>