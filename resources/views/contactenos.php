<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PlacetoPay - Contátenos</title>
    <?php
    echo view('includes/head');
    ?>
</head>

<body>

<?php
echo view('includes/nav');
?>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="margin-top-30 text-center margin-bottom-30">
                Contáctanos
            </h1>

            <p class="text-center">
                Creemos en el trabajo en equipo y queremos ser tu aliado para que tu proyecto obtenga los mejores resultados y vendas cada vez más. Ingresa tus datos para que uno de nuestros consultores te contacte y te asesore en el proceso para que puedas empezar a vender prontamente.
            </p>
        </div>
    </div>


    <form data-toggle="validator" id="contact-form" class="margin-top-30 margin-bottom-30" method="post" action="" role="form">
        <div class="messages"></div>

        <div class="controls">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="nombre" class="form-control" placeholder="Nombre Completo" required="required" data-error="Ingrese su nombre completo.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="mil" name="correo" class="form-control" placeholder="Correo electrónico" required="required" data-error="Ingrese su correo electrónico válido.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="tel" name="telefono" class="form-control" placeholder="Teléfono" required="required" data-error="Teléfono requerido.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="empresa" class="form-control" placeholder="Empresa" required="required" data-error="Empresa requerido.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="cargo" class="form-control" placeholder="Cargo" required="required" data-error="Cargo requerido.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="ciudad" class="form-control" placeholder="Ciudad" required="required" data-error="Ciudad requerido.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea id="form_message" name="message" class="form-control" placeholder="Mensaje" rows="4" required="required" data-error="Ingrese su mensaje"></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <input type="submit" class="btn btn-success btn-send" value="Enviar">
                </div>
            </div>
        </div>
    </form>

</div>

<?php
echo view('includes/footer');
?>
