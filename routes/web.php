<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/contactenos', function () {
    return view('contactenos');
});

Route::get('/tienda', function () {
    return view('tienda');
});

Route::get('/producto/{precio}', function ($precio) {
    return view('producto',  array('precio' => $precio));
});

Route::get('/pagar', function () {
    return view('index');
});

Route::get('/envato-functions-get-ip', function () {
    return EnvatoFunctions::getIp();
});

Route::get('/comprar', 'SoapController@comprar');

Route::get('/respuesta-compra', 'SoapController@respuesta');

Route::get('/confirmar-compra', 'SoapController@confirmar');

Route::get('/administrador', 'CompraController@listar');

Route::get('/actualizar-estado', 'SoapController@actualizarEstado');

