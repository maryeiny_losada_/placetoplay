<?php

namespace App\Http\Controllers;

/*
 * Clase Base para realizar petición tipo Soap
 */
class BaseSoapController extends Controller
{
    protected static $options;
    protected static $context;
    protected static $wsdl;

    public function __construct()
    {
    }

    /**
     * Instancias el Wsdl de la petición
     */
    public static function setWsdl($service)
    {
        return self::$wsdl = $service;
    }

    /**
     * Obtiene la WSDl de la petición
     */
    public static function getWsdl()
    {
        return self::$wsdl;
    }

    /**
     *
     */
    protected static function generateContext()
    {
        self::$options = [
            'http' => [
                'user_agent' => 'PHPSoapClient'
            ]
        ];
        return self::$context = stream_context_create(self::$options);
    }

    /**
     * Cambia el formato de la respuesta de xml a array
     * @param $xmlString
     * @return array
     */
    public function loadXmlStringAsArray($xmlString)
    {
        $array = (array)@simplexml_load_string($xmlString);
        if (!$array) {
            $array = (array)@json_decode($xmlString, true);
        } else {
            $array = (array)@json_decode(json_encode($array), true);
        }
        return $array;
    }
}