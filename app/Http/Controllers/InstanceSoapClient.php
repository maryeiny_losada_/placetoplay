<?php

namespace App\Http\Controllers;
use SoapClient;



class InstanceSoapClient extends BaseSoapController implements InterfaceInstanceSoap
{
    /**
     * Inicialización del la petición SOAP
     */
    public static function init()
    {
        $wsdlUrl = self::getWsdl();
        $soapClientOptions = [
            'stream_context' => self::generateContext(),
            'cache_wsdl' => WSDL_CACHE_NONE
        ];
        $soapClient = new SoapClient($wsdlUrl, $soapClientOptions);
        $soapClient->__setLocation('https://test.placetopay.com/soap/pse/');
        return $soapClient;
    }
}