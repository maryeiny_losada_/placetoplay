<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Compra;
use App\Models\Persona;
use Illuminate\Support\Facades\DB;

class CompraController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Método para listar todas las transacciones
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listar()
    {
        $compras = DB::table('compras')
            ->select('compras.*', DB::raw('(SELECT emailAddress FROM personas WHERE personas.id = compras.payer) as payer_name'), DB::raw('(SELECT emailAddress FROM personas WHERE personas.id = compras.buyer) as buyer_name'))
            ->get();

        return view('pago.lista', ['compras' => $compras]);
    }
}