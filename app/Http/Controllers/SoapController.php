<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Compra;
use App\Models\Persona;
use App\Models\Pais;
use Illuminate\Support\Facades\Cache;

/**
 * Class SoapController
 * Clase encargada de la inicialización de las peticiones
 * y diferentes métodos para realizar las transacciones
 * @package App\Http\Controllers
 *
 */

class SoapController extends BaseSoapController
{
    private $_service;
    private $_seed;
    private $_hashString;
    const LOGIN = '6dd490faf9cb87a9862245da41170ff2';
    const TRANKEY = '024h1IlD';
    const WEB_SERVICE = 'https://test.placetopay.com/soap/pse/?wsdl';

    public function __construct()
    {
        $this->_seed = date('c');
        $this->_hashString = sha1($this->_seed
            . self::TRANKEY, false);
        self::setWsdl(self::WEB_SERVICE);
        $this->_service = InstanceSoapClient::init();
    }

    /**
     * Método para comenzar con la compra, en donde se obtiene los bancos, y los paises de la base de datos,
     * tambien se cachea los bancos por 1 día
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|string
     */
    public function comprar(Request $request)
    {
        $datos = $request->input();
        $paises = Pais::all();

        if(empty($datos) == FALSE && empty($paises) == FALSE){
            try {
                if(empty(Cache::get('bancos')) == FALSE){
                    $bancos = Cache::get('bancos');
                }else{
                    //Realizar petición de lista de bancos al servicio
                    $bancos = $this->_service->getBankList(['auth' => array('login' => self::LOGIN, 'tranKey' => $this->_hashString, 'seed' => $this->_seed)]);
                }

                if(empty($bancos) == FALSE){
                    //Cacheo de los bancos
                    Cache::add('bancos', $bancos, 1440);
                    return view('pago.comprar', ['bancos' => $bancos, 'datos' => $datos, 'paises' => $paises]);
                }else{
                    $errors[0] = array('No se pudo obtener la lista de Entidades Financieras, por favor intente más tarde.');
                    return back()->with(['errors' => $errors]);
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }else{
            redirect('/');
        }
    }

    /**
     * Método encargado de recibir datos de cada transacción, los cuales son validados,
     * se crea y se almacena la información en la base de datos y se realiza la creción
     * de el pago al servicio de PSE
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirmar(Request $request)
    {
        $transaction = $request->input();
        $transactionRequest = $request->input();

        if(empty($transaction) == FALSE) {
            //Mensajes de validaciones
            $messages = [
                'required' => 'El campo :attribute es requerido.',
                'max' => 'El campo :attribute se extiende del tamaño válido.',
                'email' => 'Ingrese un correo electrónico válido.',
                'numeric' => 'El campo :attribute debe ser númerico.',
                'url' => 'Ingrese una URL válida.',
            ];

            //Validación de todos los datos del formulario
            $rules = array(
                'payer.documentType' => 'required|max:3',
                'payer.document' => 'required|max:12',
                'payer.firstName' => 'required|max:60',
                'payer.lastName' => 'required|max:60',
                'payer.company' => 'required|max:60',
                'payer.emailAddress' => 'required|max:80|email',
                'payer.address' => 'required|max:100',
                'payer.city' => 'required|max:50',
                'payer.province' => 'required|max:50',
                'payer.country' => 'required|max:2',
                'payer.phone' => 'required|max:30',
                'payer.mobile' => 'required|max:30',
                'buyer.documentType' => 'required|max:3',
                'buyer.document' => 'required|max:12',
                'buyer.firstName' => 'required|max:60',
                'buyer.lastName' => 'required|max:60',
                'buyer.company' => 'required|max:60',
                'buyer.emailAddress' => 'required|max:80|email',
                'buyer.address' => 'required|max:100',
                'buyer.city' => 'required|max:50',
                'buyer.province' => 'required|max:50',
                'buyer.country' => 'required|max:2',
                'buyer.phone' => 'required|max:30',
                'buyer.mobile' => 'required|max:30',
                'bankCode' => 'required|max:4',
                'bankInterface' => 'required|max:1',
                'description' => 'required|max:255',
                'reference' => 'required|max:32',
                'returnURL' => 'required|max:255|url',
                'language' => 'required|max:2',
                'totalAmount' => 'required|numeric',
                'taxAmount' => 'required|numeric',
                'devolutionBase' => 'required|numeric',
                'currency' => 'required|max:3',
                'tipAmount' => 'required|max:3',
                'ipAddress' => 'required|max:15',
                'userAgent' => 'required|max:255',
                '_token' => 'required',
            );

            //Creación de nombre de las variables para mensajes de error
            $niceNames = array(
                'payer.documentType' => 'Tipo de identificación del Pagador',
                'payer.document' => 'Número de identicación del Pagador',
                'payer.firstName' => 'Nombres del Pagador',
                'payer.lastName' => 'Apellidos del Pagador',
                'payer.company' => 'Empresa del Pagador',
                'payer.emailAddress' => 'Correo electrónico del Pagador',
                'payer.address' => 'Dirección del Pagador',
                'payer.city' => 'Ciudad del Pagador',
                'payer.province' => 'Departamento del Pagador',
                'payer.country' => 'País del Pagador',
                'payer.phone' => 'Teléfono fijo del Pagador',
                'payer.mobile' => 'Teléfono móvil del Pagador',
                'buyer.documentType' => 'Tipo de identificación del Comprador',
                'buyer.document' => 'Número de identicación del Comprador',
                'buyer.firstName' => 'Nombres del Comprador',
                'buyer.lastName' => 'Apellidos del Comprador',
                'buyer.company' => 'Empresa del Comprador',
                'buyer.emailAddress' => 'Correo electrónico del Comprador',
                'buyer.address' => 'Dirección del Comprador',
                'buyer.city' => 'Ciudad del Comprador',
                'buyer.province' => 'Departamento del Comprador',
                'buyer.country' => 'País del Comprador',
                'buyer.phone' => 'Teléfono fijo del Comprador',
                'buyer.mobile' => 'Teléfono móvil del Comprador',
                'bankCode' => 'Entidad Bancaria',
                'bankInterface' => 'Tipo de cuenta',
                'description' => 'Descripción',
                'reference' => 'Referencia',
                'returnURL' => 'URL de respuesta',
                'language' => 'Lenguaje',
                'totalAmount' => 'Valor total',
                'taxAmount' => 'Discriminación del impuesto',
                'devolutionBase' => 'Base de devolución',
                'currency' => 'Moneda',
                'tipAmount' => 'Propina',
                'ipAddress' => 'IP',
                'userAgent' => 'Agente de navegació',
            );

            //Se valida los datos
            $validator = \Validator::make($transaction, $rules, $messages, $niceNames);

            if ($validator->fails()) {
                //Se envian los mensajes de error de validaciones
                $errors = $validator->errors()->messages();
                return back()->with(['errors' => $errors, 'request' => $transaction]);
            }else{
                //Se valida que la entidad bancaria sea válida
                if($transaction['bankCode'] <= 0){
                    $errors[0] = array('Seleccione un Entidad bancaria válida.');
                    return back()->with(['errors' => $errors, 'request' => $transaction]);
                }

                //Se buscan las personas compradora y pagadora
                $payer = Persona::where('emailAddress', $transaction['payer']['emailAddress'])->first();;
                $buyer = Persona::where('emailAddress', $transaction['buyer']['emailAddress'])->first();;

                //Si las personas existen los datos se actualizan de los contrario se crea el registro
                if(empty($payer) == FALSE){
                    $payer->update($transaction['payer']);
                }else{
                    $payer = Persona::create($transaction['payer']);
                }

                if(empty($buyer) == FALSE){
                    $buyer->update($transaction['buyer']);
                }else{
                    $buyer = Persona::create($transaction['buyer']);
                }

                $transaction['payer'] = (int)$payer->id;
                $transaction['buyer'] = (int)$buyer->id;
                $transaction['date'] = date('Y-m-d');
                $transaction['state'] = 'pending';

                unset($transactionRequest['_token']);
                unset($transaction['_token']);

                //Se crea la compra en la base de datos
                $compra = Compra::create($transaction);

                try {
                    $auth = array('login' => self::LOGIN, 'tranKey' => $this->_hashString, 'seed' => $this->_seed);
                    $transactionRequest['returnURL'] = $transactionRequest['returnURL'].'?id='.$compra->id;
                    //Se crea la transación en el servicio
                    $create = $this->_service->createTransaction(['auth' => $auth, 'transaction' => $transactionRequest]);

                    if(empty($create) == FALSE){
                        if(strcasecmp($create->createTransactionResult->returnCode, 'SUCCESS') == 0){
                            //Se actualiza la compra en la base de datos según la respuesta del servicio
                            $returnCode = $create->createTransactionResult->returnCode;
                            $transactionID = $create->createTransactionResult->transactionID;
                            $responseCode = $create->createTransactionResult->responseCode;
                            $responseReasonText = $create->createTransactionResult->responseReasonText;
                            $url = $create->createTransactionResult->bankURL;

                            Compra::find($compra->id)->update(['returnCode' => $returnCode, 'transactionID' => $transactionID, 'responseCode' => $responseCode, 'responseReasonText' => $responseReasonText]);
                            return \Redirect::to($url);
                        }else{
                            $errors[0] = array('Error al realizar la compra. Por favor intente de nuevo.');
                            return back()->with(['errors' => $errors, 'request' => $request->input()]);
                        }
                    }else{
                        $errors[0] = array('Error al realizar la compra. Por favor intente de nuevo.');
                        return back()->with(['errors' => $errors, 'request' => $request->input()]);
                    }
                } catch (\Exception $e) {
                    $errors[0] = array($e->getMessage());
                    return back()->with(['errors' => $errors, 'request' => $request->input()]);
                }
            }
        }else{
            redirect('/tienda');
        }
    }

    /**
     * Método encargado de recibir la respuesta del servicio con respeto al pago
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function respuesta(Request $request)
    {
        if(empty($request->input()) == FALSE){
            $id_compra = $request->input()['id'];
            $compra = Compra::where('id', $id_compra)->first();

            try {
                $auth = array('login' => self::LOGIN, 'tranKey' => $this->_hashString, 'seed' => $this->_seed);
                $transactionID = $compra->transactionID;
                //Se obtiene la información de la transferencia
                $transaction = $this->_service->getTransactionInformation(['auth' => $auth, 'transactionID' => $transactionID]);
                $mensaje = $transaction->getTransactionInformationResult->responseReasonText;

                if(empty($transaction) == FALSE){
                    //Se le actualiza los datos
                    $date = date_create($transaction->getTransactionInformationResult->bankProcessDate);
                    $date->format('Y-m-d H:i:s');

                    $compra->update(['state' => $transaction->getTransactionInformationResult->transactionState, 'responseReasonText' => $transaction->getTransactionInformationResult->responseReasonText, 'dateRespuesta' => $date]);
                    return view('pago.fin', ['success' => $mensaje]);
                }else{
                    $errors[0] = array('Error al realizar la compra. Por favor intente de nuevo.');
                    return redirect('/tienda');
                }
            } catch (\Exception $e) {
                $mensaje = $e->getMessage();
                return view('pago.fin', ['error' => $mensaje]);
            }
        }else{
            redirect('/tienda');
        }
    }

    /**
     * Método para actualizar los estados
     * de las transacciones pendientes
     */
    public function actualizarEstado()
    {
        $compras = Compra::where('state', 'pending')->get();

        if(empty($compras) == FALSE){
            $auth = array('login' => self::LOGIN, 'tranKey' => $this->_hashString, 'seed' => $this->_seed);
            foreach ($compras as $compra){
                try {
                    $transactionID = $compra->transactionID;
                    $transaction = $this->_service->getTransactionInformation(['auth' => $auth, 'transactionID' => $transactionID]);
                    $mensaje = $transaction->getTransactionInformationResult->responseReasonText;

                    if(empty($transaction) == FALSE){
                        $date = date_create($transaction->getTransactionInformationResult->bankProcessDate);
                        $date->format('Y-m-d H:i:s');
                        $compra->update(['state' => $transaction->getTransactionInformationResult->transactionState, 'responseReasonText' => $transaction->getTransactionInformationResult->responseReasonText, 'dateRespuesta' => $date]);
                    }
                    continue;
                } catch (\Exception $e) {
                    continue;
                }
            }
        }
    }
}