<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Persona
 * Modelo  que hace referencia a la tabla de Personas
 * @package App\Models
 */

class Persona extends Model
{
    protected $table = 'personas';
    protected $primaryKey = 'id';
    protected $fillable = ['document', 'documentType', 'firstName',
        'lastName', 'company', 'emailAddress',
        'address',
        'city',
        'province',
        'country',
        'phone',
        'mobile'];
    public $timestamps = false;

}
