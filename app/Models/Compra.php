<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Compra
 * Modelo  que hace referencia a la tabla de Comprs
 * @package App\Models
 */

class Compra extends Model
{
    protected $table = 'compras';
    protected $primaryKey = 'id';
    protected $fillable = ['bankCode', 'bankInterface', 'returnURL',
        'reference', 'description', 'language', 'currency', 'totalAmount', 'date', 'dateRespuesta', 'state',
        'taxAmount', 'returnCode', 'transactionID', 'responseCode', 'responseReasonText',
        'devolutionBase', 'tipAmount', 'payer', 'buyer', 'shipping', 'ipAddress', 'userAgent'];
    public $timestamps = false;
}