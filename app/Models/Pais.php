<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Pais
 * Modelo  que hace referencia a la tabla de Paises
 * @package App\Models
 */

class Pais extends Model
{
    protected $table = 'paises';
    protected $primaryKey = 'id';
}
