$(document).ready(function(){
    $( "#accordion" ).accordion({
        collapsible: true
    });
    var comprador = false;

    $('.clonar-info').click(function(){
        var checked = $(this).is(':checked');
        if(checked == true){
            comprador = true;
            $(".div-comprador").hide();
            var data;
            var input;

            $(".payer").each(function(index) {
                input = $(this).data('input');
                data = $(this).val();
                $("#buyer-"+input).val(data);
            });
        }else{
            comprador = false;
            $(".div-comprador").show();

            var data;
            var input;

            $(".payer").each(function(index) {
                input = $(this).data('input');
                data = $(this).val();

                if(input != 'country' && input != 'documentType'){
                    $("#buyer-"+input).val('');
                }
            });
        }
    })

    $('#pago').submit(function (event) {
        if(comprador == true){
            var data;
            var input;

            $(".payer").each(function(index) {
                input = $(this).data('input');
                data = $(this).val();
                $("#buyer-"+input).val(data);
            });
        }

        setTimeout(function(){
            $('div#loader').css('display', 'flex');
            $("#loader").show();
        }, 500);
    })
})